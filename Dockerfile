FROM php:7.3

RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN set -x \
    && apt-get update \
    && apt-get install -y \
		lftp \
		libzip-dev \
		zip \
		unzip \
		nodejs \
		apt-transport-https \
		yarn \
		zlib1g-dev \
	&& docker-php-ext-install pdo pdo_mysql pdo_sqlite zip

# Install Composer

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
